/**
 * Variable de congiguracion sacada de la documentacion de firebase
 */
var firebaseConfig = {
    apiKey: "AIzaSyA0UGcNd9pXoGQPDfKofg3bMAxBuPVreYg",
    authDomain: "parcialbd-c1d06.firebaseapp.com",
    databaseURL: "https://parcialbd-c1d06.firebaseio.com",
    projectId: "parcialbd-c1d06",
    storageBucket: "parcialbd-c1d06.appspot.com",
    messagingSenderId: "947872369392",
    appId: "1:947872369392:web:7650d23314394bbc1c9035",
    measurementId: "G-HXS5FJ9E6Q"
};

firebase.initializeApp(firebaseConfig);
var images = [];
var dimensions = [];
var defaultText = document.getElementById("img-preview__deftext");
var previewImg = document.getElementById("img-preview__image");
var avgRGB;
var msg=document.getElementById("msg");
var success=document.getElementById("msgsuccess");



const btn = document.getElementById("btnsearch").addEventListener('click', retrieve);
const btnAudio = document.getElementById("btnaudio").addEventListener('click', retrieveAudio);
// const btnSend = document.getElementById("btnSendAll").addEventListener('click',save2send);

/**
 * Funcion que se llama al hacer click en el boton submit dde buscar imagenes,
 * recorre la base de datos verifificando los criterios de busqueda
 * para luego indexar los resultados en el html
 * 
 * @param {Event} e 
 */
function retrieve(e){
    e.preventDefault();
    let results=document.getElementById("results");

    avgRGB = getAverageRGB(document.getElementById('img-preview__image'));
    // results.innerHTML="";
    imageName = document.getElementById('search-dimension').value;
    var ref = firebase.database().ref('imagenes');
    ref.once("value").then(function(snapshot){
        var a = snapshot.val();
        // a.entries();
        images = Object.entries(a);
        // document.getElementById("myimg").src = images[0][1].URL;
        if(document.getElementById("search-dimension").value != ""){
            images.forEach(element => {
                var dim = element[1].width+"x"+element[1].height;
                if(dim == imageName){
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                }
            });
            success.innerHTML="Se han encontrado los siquientes resultados:";
            
        }
        if(document.getElementById("search-color").value != ""){
            images.forEach(element =>{
                if(compareRGB(element[1].averageRGB)){
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                }
            });
            success.innerHTML="Se han encontrado los siquientes resultados:";
        }
        if(document.getElementById("search-color").value=="" &&  document.getElementById("search-dimension").value==""){
            msg.innerHTML="Se debe ingresar uno de los parametros de busqueda";
            success.innerHTML="";
        }
    })
}

/**
 * Hace la comparación del RGB promedio en cada canal de cada una de las entradas a la base de datos con
 * la variable global del promedio RGB del preview de la imagen ingresada para hacer la busqueda.
 * 
 * en este caso se busca que en cada canal tenga un valor de mas o menos 50 para devolverlo como verdadero
 * @param {*} avg entrada de la base de datos
 */
function compareRGB(avg){
    resR = false;
    resG = false;
    resB = false;

    if((avg.r-avgRGB.r <= 50) && (avg.r-avgRGB.r >= -50)){
        resR = true;
    }
    if((avg.g-avgRGB.g <= 50) && (avg.g-avgRGB.g >= -50)){
        resG = true;
    }
    if((avg.b-avgRGB.b <= 50) && (avg.b-avgRGB.b >= -50)){
        resB = true;
    }

    if(resR && resG && resB){
        return true;
    }else{
        return false;
    }
}

/**
 * event listener del input de imagen, hace el preview de la misma
 */
document.getElementById("search-color").addEventListener('change', function(){
    const file = this.files[0];
    const reader = new FileReader;
  
    if (file)
    {
      defaultText.style.display = "none";
      previewImg.style.display = "block";

      reader.addEventListener('load', function(){
        previewImg.setAttribute('src', this.result);
      })
  
      reader.readAsDataURL(file);
    }
    
    previewImg.onload = function(){
      var canvas =  document.createElement('canvas');
      var ctx = canvas.getContext('2d');
      canvas.height = this.naturalHeight;
      canvas.width = this.naturalWidth;
      dimensions[0] = canvas.width;
      dimensions[1] = canvas.height;
      ctx.drawImage(this, 0, 0);
      const dta = canvas.toDataURL('image/jpeg');
    //   base64=dta;
    }
})

/**
 * Recibe un elemeto de imagen, en este caso el preview, y calcula el promedio color en esta usando los 
 * tres canales RGB
 * @param {HTMLImageElement} imgEl 
 */
function getAverageRGB(imgEl) {

    var blockSize = 5, // only visit every 5 pixels
        defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
        canvas = document.createElement('canvas'),
        context = canvas.getContext('2d'),
        data, width, height,
        i = -4,
        length,
        rgb = {r:0,g:0,b:0},
        count = 0;
  
    if (!context) {
        return defaultRGB;
    }
  
    height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
    width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;
  
    context.drawImage(imgEl, 0, 0);
  
    try {
        data = context.getImageData(0, 0, width, height);
    } catch(e) {
        /* security error, img on diff domain */
        msg.innerHTML="An error has oocurred with the RGB values";
        success.innerHTML="";
        return defaultRGB;
    }
  
    length = data.data.length;
  
    while ( (i += blockSize * 4) < length ) {
        ++count;
        rgb.r += data.data[i];
        rgb.g += data.data[i+1];
        rgb.b += data.data[i+2];
    }
  
    // ~~ used to floor values
    rgb.r = ~~(rgb.r/count);
    rgb.g = ~~(rgb.g/count);
    rgb.b = ~~(rgb.b/count);
  
    return rgb;
  
  }

/**
 * Funcion que se llama al hacer click en el boton submit de buscar audio,
 * recorre la base de datos verifificando los criterios de busqueda para 
 * luego indexar los resultados en el html
 * @param {Event} e 
 */
function retrieveAudio(e) {
    e.preventDefault();

    let results=document.getElementById("results");
    // results.innerHTML="";
    var audioName = document.getElementById('search-name').value;
    var ref = firebase.database().ref('imagenes');
    ref.once("value").then(function(snapshot){
        var a = snapshot.val();
        images = Object.entries(a);
        var duracion=document.getElementById("search-duration").value;
        if(duracion != ""){
            images.forEach(element => {
                if(element[1].duracionaudio == duracion){             
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                }
            });
            success.innerHTML="Se han encontrado los siquientes resultados:";
        }
        var name=document.getElementById("search-name").value;
        console.log(name);
        if(name != ""){
            
            images.forEach(element =>{

                if(name==element[1].nombre){
                    
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                }
                
            });
            success.innerHTML="Se han encontrado los siquientes resultados:";
        }

        if(name=="" &&  duracion==""){
            msg.innerHTML="Se debe ingresar uno de los parametros de busqueda"
        }
    })
    
}

  
