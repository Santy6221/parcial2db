var firebaseConfig = {
  apiKey: "AIzaSyA0UGcNd9pXoGQPDfKofg3bMAxBuPVreYg",
  authDomain: "parcialbd-c1d06.firebaseapp.com",
  databaseURL: "https://parcialbd-c1d06.firebaseio.com",
  projectId: "parcialbd-c1d06",
  storageBucket: "parcialbd-c1d06.appspot.com",
  messagingSenderId: "947872369392",
  appId: "1:947872369392:web:7650d23314394bbc1c9035",
  measurementId: "G-HXS5FJ9E6Q"
};

firebase.initializeApp(firebaseConfig);
// Create a reference with an initial file path and name
var storage = firebase.storage();
var a = document.getElementById("reproductor");
// Create a reference with an initial file path and name
var sources = [];
var description = "HiddenTribeAnthem";
var context;
var recorder;
let mixAudioRef = firebase.database().ref('mixedAudios');
var descargaAudio;
var msg=document.getElementById("msg");
var success=document.getElementById("msgsuccess");
var duration = 5000;
var chunks = [];
var audio = new AudioContext();
var mixedAudio = audio.createMediaStreamDestination();
var player = new Audio();
player.controls = "controls";
div=document.getElementById("mixedaudio")


const btn = document.getElementById("btnsearch").addEventListener('click', retrieve);

function retrieve(e){
    e.preventDefault();
    
    var ref = firebase.database().ref('imagenes');
    ref.once("value").then(function(snapshot){
        var a = snapshot.val();
        // a.entries();
        images = Object.entries(a);
        // document.getElementById("myimg").src = images[0][1].URL;
        console.log(document.getElementById("search-animal1").value);
        if(document.getElementById("search-animal1").value != "" && document.getElementById("search-animal2").value != ""){
            imageName1 = document.getElementById('search-animal1').value;
            imageName2 = document.getElementById('search-animal2').value;
            found1=false;
            found2=false;
            images.forEach(element => {
                var nombreAnimal = element[1].nombre;
                if(nombreAnimal == imageName1 && !found1){
                    let results=document.getElementById("results");
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                    found1=true;
                    sources.push(element[1].URLAudio);

                    // document.body.innerHTML+=(`<div class="card-body">`);
                }
                if(nombreAnimal == imageName2 && !found2){
                    let results=document.getElementById("results");
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                    sources.push(element[1].URLAudio);
                    found2=true;
                    // document.body.innerHTML+=(`<div class="card-body">`);
                }
            });
            
            if(!found1 || !found2){
              success.innerHTML="";
              msg.innerHTML="No se han encontrado los audios"
            }else{
              success.innerHTML="Se han encontrado los dos audios";
                let div=document.getElementById('basura');
                mixAudios();
            }
        }else{
          success.innerHTML="";
          msg.innerHTML="Se deben ingresar dos audios para ser mezclados";
        }
    })
    
    // firebase.database().ref('imagenes').on('value', function(snapshot){
    //     images = snapshot.child();
    // });
}

/**
 * Hace el fetch del audio (lo descarga desde el navegador)
 * @param {String} src Url de descarga del audio
 */
function get(src) {
  return fetch(src)
    .then(function(response) {
      return response.arrayBuffer()
    })
}

/**
 * Para la reproduccion de un audio
 * @param {Number} duration 
 * @param  {...any} media 
 */
function stopMix(duration, ...media) {
  setTimeout(function(media) {
    media.forEach(function(node) {
      node.stop()
    })
  }, duration, media)
}

/**
 * Toma la variable global sources, la cual tiene un arreglo con los enlaces de descarga de los audios,
 * realiza un fetch de los mismos, los reproduce, graba esa reproduccion como un blob, genera un url de ese blob, 
 * e indexa el audio resultante en un reproductor de audio dentro del html
 */
function mixAudios() {
  let div=document.getElementById("mixedaudio");
  Promise.all(sources.map(get)).then(function(data) {
      var len = Math.max.apply(Math, data.map(function(buffer) {
        return buffer.byteLength
      }));
      context = new OfflineAudioContext(2, len, 16000);
      return Promise.all(data.map(function(buffer) {
          return audio.decodeAudioData(buffer)
            .then(function(bufferSource) {
              var source = context.createBufferSource();
              source.buffer = bufferSource;
              source.connect(context.destination);
              return source.start()
            })
        }))
        .then(function() {
          return context.startRendering()
        })
        .then(function(renderedBuffer) {
          return new Promise(function(resolve) {
            
            var mix = audio.createBufferSource();
            mix.buffer = renderedBuffer;
            mix.connect(audio.destination);
            mix.connect(mixedAudio);              
            recorder = new MediaRecorder(mixedAudio.stream);
            recorder.start(0);
            mix.start(0);
            div.innerHTML = "playing and recording tracks..";
            // stop playback and recorder in 60 seconds
            stopMix(duration, mix, recorder);
  
            recorder.ondataavailable = function(event) {
              chunks.push(event.data);
            };
  
            recorder.onstop = function(event) {
              var blob = new Blob(chunks,  {
                "type": "audio/ogg; codecs=opus"
              });
              resolve(blob)
            };
          })
        })
        .then(function(blob) {
          div.innerHTML = "mixed audio tracks ready for download..";
          var audioDownload = URL.createObjectURL(blob);
          div.innerHTML+=(`<audio controls>  <source src="${audioDownload}" type="audio/ogg"></source> </audio>`);
          save2send(blob);

        })
    })
    .catch(function(e) {
    });
}

async function save2send(e){
    
  // enviar página al server
  // e.preventDefault();
  // SubirImagen();
  SubirAudio(e);
  // avgRGB = getAverageRGB(document.getElementById("img-preview__image"));

  var x = await resolveAfter5Seconds(10);

  let nombre = document.getElementById("search-animal1").value + document.getElementById("search-animal2").value,
  audioLink = descargaAudio;

  let newAudio=mixAudioRef.push();

  newAudio.set({
    nombre:nombre,
    URLAudio: audioLink
  });

}

function SubirAudio(blob){
  var filename = document.getElementById("search-animal1").value + document.getElementById("search-animal2").value;
  // var oggData = fs.readFileSync(document.getElementById("upload-audio").value);
  // var metadata = audioMetadata.ogg(oggData);
  var storageRef = firebase.storage().ref("/mixedAudios/" + filename);
  
  var uploadTask=storageRef.put(blob);

  uploadTask.on('state_changed',function(snapshot){
  
  },function(error) { 
    
  },function(){

    var downloadURL=uploadTask.snapshot.downloadURL;
    descargaAudio = downloadURL;
  });
}

function resolveAfter5Seconds(x) { 
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x);
    }, 5000);
  });
}


