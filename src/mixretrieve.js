var firebaseConfig = {
    apiKey: "AIzaSyA0UGcNd9pXoGQPDfKofg3bMAxBuPVreYg",
    authDomain: "parcialbd-c1d06.firebaseapp.com",
    databaseURL: "https://parcialbd-c1d06.firebaseio.com",
    projectId: "parcialbd-c1d06",
    storageBucket: "parcialbd-c1d06.appspot.com",
    messagingSenderId: "947872369392",
    appId: "1:947872369392:web:7650d23314394bbc1c9035",
    measurementId: "G-HXS5FJ9E6Q"
};
  
firebase.initializeApp(firebaseConfig);

const btn = document.getElementById("btnsearch").addEventListener('click', retrieve);
var msg=document.getElementById("msg");
var success=document.getElementById("msgsuccess");

/**
 * Funcion que se llama al hacer click en el boton submit de buscar un audio mezclado,
 * recorre la base de datos verifificando los criterios de busqueda para luego indexar 
 * los resultados en el html
 * @param {Event} e 
 */
function retrieve(e){
    e.preventDefault();


    imageName = document.getElementById('search-mix').value;
    var ref = firebase.database().ref('mixedAudios');
    ref.once("value").then(function(snapshot){
        var a = snapshot.val();
        let found=false;
        // a.entries();
        images = Object.entries(a);
        // document.getElementById("myimg").src = images[0][1].URL;
        if(document.getElementById("search-mix").value != ""){
            images.forEach(element => {
                if(element[1].nombre == imageName){
                    found=true;
                    let results=document.getElementById("results");
                    results.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    results.lastChild.innerHTML+=(`<div class="card" style="width: 400px; margin: auto; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px;">`);
                    // results.lastChild.innerHTML+=(`<img src="${element[1].URL}" class="card-img-top img-thumbnail" alt="..." style="border-radius: 5px 5px 0 0; width: 400px; height: auto;">`);
                    results.lastChild.innerHTML+=(`<audio controls>  <source src="${element[1].URLAudio}" type="audio/ogg"></source> </audio>`);
                    results.lastChild.innerHTML+=(`<p class="card-text">${element[1].nombre} </p>`);
                    results.lastChild.innerHTML+=(`</div>`);
                }
            });
            if(found){
                success.innerHTML="Se ha encontrado el audio";
            }else{
                msg.innerHTML="No se ha encontrado el audio";
            }
        }else{
            msg.innerHTML="Se debe ingresar el nombre";
        }
    })
    
    // firebase.database().ref('imagenes').on('value', function(snapshot){
    //     images = snapshot.child();
    // });
}