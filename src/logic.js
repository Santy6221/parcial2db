/**
 * Variable de congiguracion sacada de la documentacion de firebase
 */
var firebaseConfig = {
  apiKey: "AIzaSyA0UGcNd9pXoGQPDfKofg3bMAxBuPVreYg",
  authDomain: "parcialbd-c1d06.firebaseapp.com",
  databaseURL: "https://parcialbd-c1d06.firebaseio.com",
  projectId: "parcialbd-c1d06",
  storageBucket: "parcialbd-c1d06.appspot.com",
  messagingSenderId: "947872369392",
  appId: "1:947872369392:web:7650d23314394bbc1c9035",
  measurementId: "G-HXS5FJ9E6Q"
};

// var audioMetaData = require('audio-metadata'),
//     fs = require('fs');
//API key, database URL,etc.

var msg=document.getElementById("msg");
var success=document.getElementById("msgsuccess");
var Imagen;
var descarga;
var defaultText = document.getElementById("img-preview__deftext");
var previewImg = document.getElementById("img-preview__image");
var base64;
var dimensions = [];
var avgRGB;
var audio;
var descargaAudio;
var audioduration;
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const btnSend = document.getElementById("btnSendAll").addEventListener('click',save2send);
let imgref = firebase.database().ref('imagenes');

function resolveAfter2Seconds(x) { 
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x);
    }, 2000);
  });
}


/**
 * Funcion que se llama al hacer click en el boton de submit del formulario, se hace la verificación por tamaño
 * de imagen a ingresar a la base de datos y centraliza el proceso de ingresar las cosas a la base de datos
 * @param {event} e 
 */
async function save2send(e){
  e.preventDefault();
  if(dimensions[0]<350 || dimensions[1]<350){
    msg.innerHTML="";
    console.log("Noup");
    msg.innerHTML="Las dimensiones de la imagen deben ser mayores a 350x350 px";
  }else{  // enviar página al server
    
    SubirImagen();
    SubirAudio();
    avgRGB = getAverageRGB(document.getElementById("img-preview__image"));
  
    var x = await resolveAfter2Seconds(10);
  
    let nombre =document.getElementById("name").value,
    categoria =document.getElementById("category").value,
    etiquetas =document.getElementById("tags").value,
    autor =document.getElementById("author").value,
    foto = document.getElementById("image").value,
    autoraudio=document.getElementById("authoraudio").value,
    duracion=audioduration;
  
    let newimage=imgref.push();
    try {
      newimage.set({
        nombre:nombre,
        categoria:categoria,
        etiquetas:etiquetas,
        autor:autor,
        URL:descarga,
        width:dimensions[0],
        height:dimensions[1],
        encript:base64,
        averageRGB:avgRGB,
        URLAudio: descargaAudio,
        authoraudio: autoraudio,
        duracionaudio: audioduration
      });
      msg.innerHTML="";
      success.innerHTML="Los datos se han ingresado correctamente en la base de datos";
    } catch (error) {
      msg.innerHTML=`An error has ocurred: ${error}`;
    }
  }
}

/**
 * Event listener del div del input de la imagen, se verifica que exista un archivo para despues hacer un
 * preview de la misma
 */
document.getElementById("image").addEventListener('change', function(){
  const file = this.files[0];
  const reader = new FileReader;
  Imagen=event.target.files[0];

  if (file)
  {
    defaultText.style.display = "none";
    previewImg.style.display = "block";

    reader.addEventListener('load', function(){
      previewImg.setAttribute('src', this.result);
    })

    reader.readAsDataURL(file);
  }

  previewImg.onload = function(){
    var canvas =  document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    canvas.height = this.naturalHeight;
    canvas.width = this.naturalWidth;
    dimensions[0] = canvas.width;
    dimensions[1] = canvas.height;
    ctx.drawImage(this, 0, 0);
    const dta = canvas.toDataURL('image/jpeg');
    base64=dta;
  }

})

/**
 * Hace la logica de subir la imagen a firebase usando la variable global de imagen
 */
function SubirImagen(){
  var filename = Imagen.name;
  var storageRef = firebase.storage().ref("/images/" + filename);
  
  var uploadTask=storageRef.put(Imagen);

  uploadTask.on('state_changed',function(snapshot){
  
  },function(error) { 
    
  },function(){

    var downloadURL=uploadTask.snapshot.downloadURL;
    descarga = downloadURL;
  });
}


/**
 * Para la obtención de los metadatos del audio, encontramos un código de ejemplo en el que se obtiene 
 * la duración de este mediante javascript, con las librerías de file reader y audio context.
 * 
 * https://ourcodeworld.com/articles/read/1036/how-to-retrieve-the-duration-of-a-mp3-wav-audio-file-in-the-browser-with-javascript
 */
// Add a change event listener to the file input
document.getElementById("upload-audio").addEventListener('change', function(event){
  audio=event.target.files[0];
  // Obtain the uploaded file, you can change the logic if you are working with multiupload
  var file = this.files[0];
  
  // Create instance of FileReader
  var reader = new FileReader();

  // When the file has been succesfully read
  reader.onload = function (event) {
      // Create an instance of AudioContext
      var audioContext = new (window.AudioContext || window.webkitAudioContext)();
      // Asynchronously decode audio file data contained in an ArrayBuffer.
      audioContext.decodeAudioData(event.target.result, function(buffer) {
          // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
          var duration = buffer.duration;
          audioduration=duration;

      });
  };

  // In case that the file couldn't be read
  reader.onerror = function (event) {
      console.error("An error ocurred reading the file: ", event);
      msg.innerHTML+=`An error ocurred reading the file: ${event}`
  };

  // Read file as an ArrayBuffer, important !
  reader.readAsArrayBuffer(file);
}, false);


/**
 * Hace la logica de subir la imagen a firebase usando la variable global de audio
 */
function SubirAudio(){
  var filename = audio.name;
  var duracion = audio.duration;
  // var oggData = fs.readFileSync(document.getElementById("upload-audio").value);
  // var metadata = audioMetadata.ogg(oggData);
  var storageRef = firebase.storage().ref("/audios/" + filename);
  
  var uploadTask=storageRef.put(audio);

  uploadTask.on('state_changed',function(snapshot){
  
  },function(error) { 
    
  },function(){

    var downloadURL=uploadTask.snapshot.downloadURL;
    descargaAudio = downloadURL;
  });
}

/**
 * Recibe un elemeto de imagen, en este caso el preview, y calcula el promedio color en esta usando los 
 * tres canales RGB
 * @param {HTMLImageElement} imgEl 
 */
function getAverageRGB(imgEl) {

  var blockSize = 5, // only visit every 5 pixels
      defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
      canvas = document.createElement('canvas'),
      context = canvas.getContext && canvas.getContext('2d'),
      data, width, height,
      i = -4,
      length,
      rgb = {r:0,g:0,b:0},
      count = 0;

  if (!context) {
      return defaultRGB;
  }

  height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
  width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

  context.drawImage(imgEl, 0, 0);

  try {
      data = context.getImageData(0, 0, width, height);
  } catch(e) {
      /* security error, img on diff domain */
      msg.innerHTML="An error has oocurred with the RGB values";
      return defaultRGB;

  }

  length = data.data.length;

  while ( (i += blockSize * 4) < length ) {
      ++count;
      rgb.r += data.data[i];
      rgb.g += data.data[i+1];
      rgb.b += data.data[i+2];
  }

  // ~~ used to floor values
  rgb.r = ~~(rgb.r/count);
  rgb.g = ~~(rgb.g/count);
  rgb.b = ~~(rgb.b/count);

  return rgb;

}


/**
 * 
 */
document.getElementById("upload-audio").addEventListener("change",function(event){
  audio=event.target.files[0];
  const reader = new FileReader;

  if (audio)
  {

    reader.addEventListener('load', function(){
      // audio.setAttribute('src', this.result);
    })

  }
})

